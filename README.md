# VPC Peering in AWS

## Introduction:
AWS (Amazon Web Services) is a cloud-based platform offering various services to help businesses operate efficiently. One such service is VPC (Virtual Private Cloud) peering, which allows different VPCs to communicate with each other. VPC peering simplifies the networking process, allowing businesses to create a secure, isolated environment.

## What is VPC Peering?
VPC Peering is a networking connection between two VPCs that enables them to communicate with each other. It allows VPCs to share resources such as data, applications, and other services. VPC peering enables secure and private communication between different VPCs without exposing their resources to the internet. AWS VPC peering allows you to connect two VPCs, even if they are in different regions.

![Vpc Peering in aws](./vpc.png)

### Step by Step Process of VPC Peering in AWS:
Here are the steps to follow to set up VPC peering in AWS:

**Step 1: Create a VPC**
The first step is to create a VPC. You can create a VPC using the AWS Management Console or AWS CLI. While creating a VPC, specify a unique CIDR block that is not overlapping with any other VPC.

**Step 2: Create a VPC Peer**
To create a VPC peer, go to the VPC dashboard and select the VPC that you want to peer. From the Actions drop-down menu, select "Create Peering Connection." Enter the Peering Connection Name, select the VPC that you want to peer with, and click "Create Peering Connection."

**Step 3: Accept the Peering Connection**
Once the peering connection is created, go to the VPC dashboard and select the VPC that you want to accept the connection. From the Actions drop-down menu, select "Accept Peering Connection." This will accept the peering connection request and allow the VPCs to communicate with each other.

**Step 4: Configure Routing**
The next step is to configure the routing between the two VPCs. You need to add a route to each VPC's route table. For the VPC that initiated the peering connection, add a route to the destination CIDR block of the other VPC, using the peering connection ID as the target. Similarly, for the other VPC, add a route to the destination CIDR block of the first VPC, using the peering connection ID as the target.

**Step 5: Test Connectivity**
The final step is to test the connectivity between the VPCs. You can use any tool to test the connectivity, such as ping or telnet.

### Conclusion:
VPC peering is a powerful tool that simplifies networking and allows businesses to create a secure, isolated environment. The above steps will help you set up VPC peering in AWS and enable two VPCs to communicate with each other. With VPC peering, businesses can share resources, reduce costs, and improve overall efficiency.

Author: [somaymangla](https://www.linkedin.com/in/er-somay-mangla/)



#Gitlab #vpc #aws #cloud #awscloud #devops #somaymangla #linux 
